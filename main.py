import logging

from aiogram import executor
from aiogram.dispatcher.filters import Text

from config import dp
from handlers.about_us import about_us
from handlers.bot_by_orders import register_survey_handlers
from handlers.choose_clothes import clothes, man_clothes, woman_clothes
from handlers.start import start

if __name__ == "__main__":
    dp.register_message_handler(start, commands=["start"])
    dp.register_message_handler(clothes, commands=["clothes"])
    dp.register_message_handler(man_clothes, Text("Мужские"))
    dp.register_message_handler(woman_clothes, Text("Женские"))
    dp.register_callback_query_handler(about_us, Text('about'))

    # опросник
    register_survey_handlers(dp)

    executor.start_polling(dp)
