from aiogram import types


async def clothes(message: types.Message):
    kb = types.ReplyKeyboardMarkup()
    kb.add(
        types.KeyboardButton("Женские"),
        types.KeyboardButton("Мужские")
    )
    await message.answer(
        "Выберите одежду в зависимости от вашего пола:",
        reply_markup=kb
    )


async def man_clothes(message: types.Message):
    await message.answer("Костюмы, "
                         "Рубашки, "
                         "Штаны")


async def woman_clothes(message: types.Message):
    await message.answer("Платья, "
                         "Рубашки, "
                         "Штаны")
