from pprint import pprint

from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup


class Bot_By_Orders(StatesGroup):
    name = State()
    age = State()
    location = State()
    gender = State()


async def start_survey(message: types.Message):
    await Bot_By_Orders.name.set()
    await message.reply("Как вас зовут?")


async def proccess_name(message: types.Message, state: FSMContext):
    send_name = message.text
    if not send_name.isalpha():
        await message.answer("Введите ваше имя буквами!")
    elif len(str(send_name)) < 2 or len(str(send_name)) > 34:
        await message.answer("Введите имя не менее 2 букв и не более 34")
    else:
        async with state.proxy() as data:
            data["name"] = str(send_name)
            pprint(data.as_dict())
        await message.answer("Очень приятно познакомиться.")

    await Bot_By_Orders.next()
    await message.reply("Сколько вам лет?")


async def procces_age(message: types.Message, state: FSMContext):
    send_age = message.text
    if not send_age.isdigit():
        await message.answer("Введите ваш возраст ЦИФРАМИ!")
    elif int(send_age) < 16 or int(send_age) > 100:
        await message.answer(
            "Наша компания принимает заказы только с 16 до 100 лет."
        )
    else:
        async with state.proxy() as data:
            data["age"] = int(send_age)
            pprint(data.as_dict())
        await message.answer("Вы,соответствуете возрастным требованиям.")

    await Bot_By_Orders.next()
    await message.reply("Где вы находитесь?")


async def procces_location(message: types.Message, state: FSMContext):
    send_location = message.text
    if not send_location.isalnum():
        await message.answer(
            "Введите название улицы буквами и номер дома цифрами!"
        )
    else:
        async with state.proxy() as data:
            data["location"] = send_location
            pprint(data.as_dict())
        await message.answer("Ваш заказ уже скоро будет доставлен")


async def gender(message: types.Message):
    kb = types.ReplyKeyboardMarkup()
    kb.add("Мужской", "Женский")
    await Bot_By_Orders.next()
    await message.reply("Введите пол:", reply_markup=kb)


async def process_gender(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['gender'] = message.text
        await state.finish()


def register_survey_handlers(dp: Dispatcher):
    dp.register_message_handler(start_survey, commands=['surv'])
    dp.register_message_handler(proccess_name, state=Bot_By_Orders.name)
    dp.register_message_handler(procces_age, state=Bot_By_Orders.age)
    dp.register_message_handler(process_gender, state=Bot_By_Orders.gender)
    dp.register_message_handler(procces_location, state=Bot_By_Orders.location)
