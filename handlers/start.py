import os
import random

from aiogram import types

from config import bot
from handlers.constants import HELLO_TEXT


# @dp.message_handler(commands=["start"])
async def start(message: types.Message):
    """Привествует пользователя"""
    print(f"{message.from_user=}")

    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton("Наш сайт:",
                                   url="https://lirus.kg/"),
        types.InlineKeyboardButton(
            "Инстаграм:",
            url="https://www.instagram.com/lirus.kg/")

    )
    kb.add(types.InlineKeyboardButton("О нас:", callback_data='about'))
    await message.reply(HELLO_TEXT, reply_markup=kb)
